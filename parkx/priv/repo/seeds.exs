## Script for populating the database. You can run it as:
##
##     mix run priv/repo/seeds.exs
##
## Inside the script, you can read and write to any of your
## repositories directly:
##
##     Parkx.Repo.insert!(%Parkx.SomeSchema{})
##
## We recommend using the bang functions (`insert!`, `update!`
## and so on) as they will fail if something goes wrong.
#alias Parkx.{Repo,Entities.User,Entities.Parking}
#
#[%{name: "Fred Flintstone", username: "fred", hashed_password: "parool", email: "abc@gmail.com", credit: 100, monthly_selected: false},
#  %{name: "Hariti Sanchaniya", username: "hariti", hashed_password: "parool", email: "abc@gmail.com", credit: 2, monthly_selected: false}]
#|> Enum.map(fn user_data -> User.changeset(%User{}, user_data) end)
#|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
#
#[%{title: "Tartu linn, A piirkond", dest_name: "Riia, Tartu (Tartu linn), 51003", lattitude: 58.37665859938617, longitude: 26.726136803627014, zone: "A", status: "free"},
#  %{title: "Tartu linn, A piirkond", dest_name: "Vanemuise 4, Tartu, 51003", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "A", status: "free"},
#  %{title: "Tartu linn, A piirkond", dest_name: "Ulikooli 2, Tartu, 51003", lattitude: 58.37733646334772, longitude: 26.724935173988342, zone: "A", status: "free"},
#  %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 25, Tartu, 51007", lattitude: 58.38502944712024, longitude: 26.710392236709595, zone: "B", status: "free"},
#  %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: 58.384990078073955, longitude: 26.710333228111267, zone: "B", status: "free"},
#  %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 20, Tartu, 51007", lattitude: 58.38463856678448, longitude: 26.71097159385681, zone: "B", status: "free"}
#]
#|> Enum.map(fn user_data -> Parking.changeset(%Parking{}, user_data) end)
#|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

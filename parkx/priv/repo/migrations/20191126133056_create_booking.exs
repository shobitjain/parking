defmodule Parkx.Repo.Migrations.CreateBooking do
  use Ecto.Migration

  def change do
    create table(:bookings) do
      add :start_time, :string
      add :end_time, :string
      add :charge, :float
      add :payment_status, :string
      add :status, :string
      add :type, :string
      add :parking_id, references(:parking)
      add :user_id, references(:users)

      timestamps()
    end
  end
end

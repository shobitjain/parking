defmodule Parkx.Repo.Migrations.CreateUser do
  use Ecto.Migration
  def change do
    create table(:users) do
      add :name, :string
      add :username, :string
      add :hashed_password, :string
      add :email, :string
      add :credit, :float
      add :monthly_selected, :boolean

      timestamps()
    end

    create unique_index(:users, [:username])

  end
end

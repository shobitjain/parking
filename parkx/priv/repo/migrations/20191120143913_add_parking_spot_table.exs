defmodule Parkx.Repo.Migrations.AddParkingSpotTable do
  use Ecto.Migration

  def change do
    create table(:parking) do
      add :title, :string
      add :dest_name, :string
      add :lattitude, :float
      add :longitude, :float
      add :zone, :string
      add :status, :string

      timestamps()
    end
  end
end

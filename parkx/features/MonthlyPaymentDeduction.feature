Feature: Deduct monthly charge from user credit for realtime payment

Scenario: Monthly charge from user credit deducted on 1st of every month
    Given user is already registered with Parking System
    And user is logged into the system
    And user has configured monthly payment option for realtime payment
    And user makes some real time booking throughout the month
    When 1st of every month comes
    Then user credit will be updated with deducted monthly charge
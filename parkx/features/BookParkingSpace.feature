Feature: Book Parking Space for User

    Scenario:user select payment type as 'Hourly Payment' and makes booking
        Given user is logged into the system
        And enterd destination for parking
        And clicked search button
        And user is displayed available parking options in tabular form
        When user selects payment type for booking as 'Hourly Payment'
        And clicks on the 'Book' button
        Then the user is displayed booking success message
        And parking space status is changed to busy
        And Parking cost for an hour is deducted from user's credit
        And user can see his booking details in the My Bookings page

    Scenario:user select payment type as 'Real Time Payment' and makes booking
        Given user is logged into the system
        And enterd destination for parking
        And clicked search button
        And user is displayed available parking options in tabular form
        When user selects payment type for booking as 'Real Time Payment'
        And clicks on the 'Start Booking' button
        Then the user is displyed booking success message
        And parking space status is changed to busy
        And user can see his booking details in the My Bookings page

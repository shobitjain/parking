Feature: Cost Estimation of parking space for users

#Cost Estimation
 Scenario: user enters the endTime which is greater than current time
    Given user is logged into the system
    And enterd destination for parking
    And clicked search button
    When user enters <"endTime"> for parking which is greater than current time
    Then user is displayed hourly and real time cost for parking in tabular form
    And on map inteface

 Scenario: user enters the endTime which is less than current time
    Given user is logged into the system
    And enterd destination for parking
    And clicked search button
    When user enters <"endTime"> for parking which is less than current time
    Then user is displayed an error pop-up 'Please enter valid end time'
    And on map inteface

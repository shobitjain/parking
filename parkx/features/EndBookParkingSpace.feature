Feature: End the Real Time booking of a parking space for a user

    Scenario:user has disabled the Monthly Payment option and ends the 'Real Time Booking'
        Given user is logged into the system
        And enterd destination for parking
        And clicked search button
        And user is displayed available parking options in tabular form
        And user selects payment type for booking as 'Real Time Payment'
        And clicks on the 'Start Booking' button
        When user clicks on the 'End Booking' button
        Then the user is displyed end booking success message
        And parking space status is changed to free
        And And Parking cost for the booking is deducted immediately from user's credit
        And Payment status for the booking is updated to Paid
        And user can see his booking details in the My Bookings page

    Scenario:user has enabled the Monthly Payment option and ends the 'Real Time Booking'
        Given user is logged into the system
        And enterd destination for parking
        And clicked search button
        And user is displayed available parking options in tabular form
        And user selects payment type for booking as 'Real Time Payment'
        And clicks on the 'Start Booking' button
        When user clicks on the 'End Booking' button
        Then the user is displyed 'End Booking Success' message
        And parking space status is changed to free
        And Payment status for the booking is remains Unpaid
        And user can see his booking details in the My Bookings page

    

Feature: Search a parking space based on geolocation for users

#Search Parking Space geolocation
Scenario: system detects location and gives option for available parkings
    Given user is logged into the System
    And user is on search parking space page
    When user clicks on text box to srearch parking space
    And system detects geolocation of user
    Then user is displayed available parking options in tabular form
    And on map inteface
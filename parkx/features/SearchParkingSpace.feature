Feature: Search a parking space for users

#Search Parking Search
Scenario:user enters valid destination location
    Given user is logged into the System
    When user enters valid <"area"> for a parking space
    And click on search button
    Then user is displayed available parking options in tabular form
    And on map inteface

Scenario:user enters invalid destination location
    Given user is logged into the System
    When user enters invalid <"area"> for a parking space
    And click on search button
    Then user is displayed an error pop-up 'Invalid Address Entered'
    And on map inteface
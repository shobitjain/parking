Feature: Authentication Feature

#Log In - Successful
Scenario:UserLogIn_success
    Given user is already registered with Parking System
    When user enters correct <"username"> with correct <"password">
    And the user clicks the Login button
    Then user should be authenticated to home screen

#Log In - Failure	
Scenario:UserLogIn_failure
	Given user is already registered with Parking System
	When user enters correct <"username"> with incorrect <"password">
	And the user clicks the Login button
	Then user should get an error message

#Register User - Successful
Scenario:UserRegistration_success
    Given user has launched the website
    When user enters registration form
    And user clicks submit
    Then user gets a registration successful message
    And user should bookParkingForUsers redirected to the Login page
	
#Register user - Failure
Scenario:UserRegistration_failure
    Given user has launched the website
    When user enters registration form with already existing username
    And user clicks submit
    Then user gets a registration successful message
    And user should get error message as <"Username already exists">
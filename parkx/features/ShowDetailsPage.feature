Feature: Show Selected Parking Space Details Page

Scenario:user clicks the parking space link from populated table
    Given user is logged into the system
    And enterd destination for parking
    And clicked search button
    And user is displayed available parking options in tabular form
    When user clicks the particular parking space link from the populated table
    Then user is redirected to parking space details page for that particular parking space
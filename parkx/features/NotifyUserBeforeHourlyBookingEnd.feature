Feature: End the Hourly booking of a parking space for a user

    Scenario:user has started the hourly booking and extend the booking
        Given user is logged into the system
        And enterd destination for parking
        And clicked search button
        And user is displayed available parking options in tabular form
        And user selects payment type for booking as 'Hourly Payment'
        And clicks on the 'Book' button
        When endTime reaches 10 minutes to the current time
        And User get notification to extend the booking
        And User gets extend booking option
        And User clicks on extend button
        And User enters extened hours in the text box
        And User clicks "extend" button
        And User gets extention success message

    Scenario:user has started the hourly booking and system ends the booking
        Given user is logged into the system
        And enterd destination for parking
        And clicked search button
        And user is displayed available parking options in tabular form
        And user selects payment type for booking as 'Hourly Payment'
        And clicks on the 'Book' button
        And endTime reaches 10 minutes to the current time
        And User get notification to extend the booking
        When endTime reaches 2 minutes to the current time
        User booking ends
        Parking space becomes free and available for next bookings

# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :parkx,
  ecto_repos: [Parkx.Repo]

# Configures the endpoint
config :parkx, ParkxWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "MgnOyyT0CXLPQjCWzOOoTDzWaCSqR0UqqJC3NKQMmK/LYtyqgTQe7yg7khKTyhrw",
  render_errors: [view: ParkxWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Parkx.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Configure Google Maps
config :google_maps,
  api_key: "AIzaSyDESWns7Zg6NAaLrlX0P2zRfNkJ3eBfsrE"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
config :parkx, Parkx.Auth.Guardian,
       issuer: "parkx",
       secret_key: "jlTexmXz6/isYwbx04aRwo4f7PuVL6WrMK5a0dSe3eQtxdn6+Xpai/9hEEKJBvvR" # put the result of running `mix guardian.gen.secret`

config :parkx, Parkx.Scheduler,
       jobs: [
         {"*/1 * * * *", {Parkx.Task, :work, []}},
         {"0 0 1 * *", {Parkx.Task, :monthlywork, []}}
#         0 0 1 * * //add this cron for monthly
       ]

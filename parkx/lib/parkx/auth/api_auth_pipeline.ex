defmodule Parkx.Auth.ApiAuthPipeline do
  use Guardian.Plug.Pipeline,
      otp_app: :parkx,
      error_handler: Parkx.Auth.ApiErrorHandler,
      module: Parkx.Auth.Guardian

  plug Guardian.Plug.VerifyHeader, realm: "Bearer"
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource
end
defmodule Parkx.Task do
  use Timex

  import Ecto.Query, only: [from: 2]
  alias Parkx.Repo
  alias Ecto.{Changeset, Multi}
  alias Parkx.{Repo, Entities.User, Model.Helper, Entities.Parking, Entities.Booking}

  def work do
    query = from b in Booking,
                 where: b.type == "hourly" and b.status == "ongoing",
                 select: b
    bookings = Repo.all(query)
    params = %{}
    currTime = DateTime.utc_now() |> Timex.shift(minutes: 120)
    newcurrTime = DateTime.utc_now() |> Timex.shift(minutes: 120)

   case bookings do
     [] ->
        IO.inspect "No booking available for end"
      _ ->
        bookings |> Enum.map( fn booking -> finddiff(booking,currTime) end)
        bookings |> Enum.map( fn booking -> finddifften(booking,newcurrTime) end)
    end
  end

  def finddiff(bookingdata,currTime) do
    endTime =  Timex.parse!(bookingdata.end_time, "{ISO:Extended:Z}")
    mindiff = Timex.diff(currTime,endTime, :seconds)
    IO.inspect "********************"
    IO.inspect mindiff
    IO.inspect "********************"
    parkingId  = bookingdata.parking_id
    parking = Repo.one(from p in Parking, where: p.id == ^parkingId)
    booking = Repo.one(from b in Booking, where: b.id == ^bookingdata.id)
    if mindiff >= -120 do
      parkingUpdated = parking |> Parking.changeset(%{status: "free"}) |> Repo.update!()
      bookingUpdated = booking |> Booking.changeset(%{status: "completed"}) |> Repo.update!()
    else
      IO.inspect "no booking to end"
    end
    end

  def finddifften(bookingdata,newcurrTime) do
    endTime =  Timex.parse!(bookingdata.end_time, "{ISO:Extended:Z}")
    mindiff = Timex.diff(newcurrTime,endTime, :seconds)
    IO.inspect "********************"
    IO.inspect mindiff
    IO.inspect "********************"
    booking = Repo.one(from b in Booking, where: b.id == ^bookingdata.id)
    params = %{}
    if mindiff > -600 do
      ParkxWeb.Endpoint.broadcast("links", "requests", params |> Map.put(:booking_id, booking.id) |> Map.put(:user_id, booking.user_id))
      IO.inspect "Broadcast done"
    else
      IO.inspect "no booking to extend"
    end
  end

  def monthlywork do
    query = from b in Booking,
                 where: b.type == "realtime" and b.payment_status == "unpaid" and b.status == "completed",
                 select: b
    bookings = Repo.all(query)

    bookings |> Enum.map( fn booking -> updateMonthly(booking) end)

    end

    def updateMonthly(booking) do
    case booking do
      [] ->
        IO.inspect "No booking available #"
      _ ->
        IO.inspect "IN MOnthly #######################"
        query = from u in User,
                     where: u.id == ^booking.user_id,
                     select: u
        user = Repo.all(query)
        newCredit = hd(user).credit - booking.charge
        userUpdated = hd(user) |> User.changeset(%{credit: newCredit}) |> Repo.update!()
        bookings = booking |> Booking.changeset(%{payment_status: "paid"}) |> Repo.update!()
    end
  end
  end

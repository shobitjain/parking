defmodule Parkx.Entities.Booking do
  use Ecto.Schema
  import Ecto.Changeset

  schema "bookings" do
    field :start_time, :string
    field :end_time, :string
    field :charge, :float
    field :payment_status, :string
    field :status, :string
    field :type, :string
    belongs_to :user, Parkx.Entities.User
    belongs_to :parking, Parkx.Entities.Parking

    timestamps()
  end

  @doc false
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:start_time, :end_time, :charge, :payment_status, :status, :type])
  end

end

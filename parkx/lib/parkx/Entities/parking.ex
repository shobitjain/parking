defmodule Parkx.Entities.Parking do
  use Ecto.Schema
  import Ecto.Changeset

  schema "parking" do
    field :title, :string
    field :dest_name, :string
    field :lattitude, :float
    field :longitude, :float
    field :zone, :string
    field :status, :string
    has_many :boooking, Parkx.Entities.Booking

    timestamps()
  end

  @doc false
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :dest_name, :lattitude, :longitude, :zone, :status])
  end


end

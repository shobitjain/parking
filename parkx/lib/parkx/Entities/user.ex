defmodule Parkx.Entities.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :name, :string
    field :username, :string
    field :hashed_password, :string
    field :email, :string
    field :credit, :float
    field :monthly_selected, :boolean
    has_many :boooking, Parkx.Entities.Booking

    timestamps()
  end

  @doc false
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :username, :hashed_password, :email, :credit, :monthly_selected])
    |> validate_required([:name, :username])
    |> unique_constraint(:username)
    |> hash_password
  end

  defp hash_password(%Ecto.Changeset{valid?: true, changes: %{hashed_password: password}} = changeset) do
    change(changeset, hashed_password: Pbkdf2.hash_pwd_salt(password))
  end
  defp hash_password(changeset), do: changeset

end

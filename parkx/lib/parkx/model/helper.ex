defmodule Parkx.Model.Helper do
  use Timex
  def getDistance(lat1, lng1, lat2, lng2) do
    coord1 = {lat1, lng1}
    coord2 = {lat2, lng2}
    case (GoogleMaps.distance(coord1, coord2)) do
      {:ok, resp} ->
        [head | _tail] = resp["rows"]
        [h1 | _tail] = head["elements"]
        h1["distance"]["value"]
      {:error, resp} ->
        resp
    end
  end

  def helper(spot, hourCycle, minuteCycle) do
    case spot do
      %{zone: "A"} ->
        spot |> Map.put("HourlyCharge", 2*hourCycle) |> Map.put("RealTimeCharge", 0.16 * minuteCycle)
      %{zone: "B"} ->
        spot |> Map.put("HourlyCharge", 1*hourCycle) |> Map.put("RealTimeCharge", 0.08 * minuteCycle)
    end
  end

  def addPrice(spots, endTime) do
    case endTime do
      "" ->
        %{status: "ok", tabledata: spots}
      _ ->
        curr = DateTime.utc_now() |> Timex.shift(minutes: 120)
        finish = Timex.parse!(endTime, "{ISO:Extended:Z}") |> Timex.shift(minutes: 120)

        diff = Timex.diff(finish, curr, :minutes)
        hourCycle = diff/60 |> Float.ceil |> trunc
        minuteCycle = diff/5 |> Float.ceil |> trunc
        case (minuteCycle >= 0) do
          true ->
            updated_spots = spots |> Enum.map(fn spot -> helper(spot, hourCycle, minuteCycle) end)
            %{status: "ok", tabledata: updated_spots}
          false ->
            updated_spots = spots |> Enum.map(fn spot -> helper(spot, 0, 0) end)
            %{status: "EndTime error", tabledata: updated_spots}
        end

    end
  end

  def changeDestLatLong(spot) do
    %{lattitude: x, longitude: y, dest_name: z} = spot
    spot
    |> Map.put(:lat, x) |> Map.put(:lng, y) |> Map.put(:Parking_spot, z)
    |> Map.delete(:lattitude) |> Map.delete(:longitude) |> Map.delete(:dest_name)
  end

end

defmodule Parkx.Repo do
  use Ecto.Repo,
      otp_app: :parkx,
      adapter: Ecto.Adapters.Postgres
end

defmodule ParkxWeb.Router do
  use ParkxWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :browser_auth do
    plug Parkx.Auth.ApiAuthPipeline
  end

  pipeline :ensure_auth do
    plug Guardian.Plug.EnsureAuthenticated
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ParkxWeb do
    pipe_through [:browser]
    get "/", PageController, :index
  end

  pipeline :auth_api do
    plug Parkx.Auth.ApiAuthPipeline
  end

  scope "/api", ParkxWeb do
    pipe_through [:api]
    post "/users", UserAPIController, :create
    post "/sessions", SessionAPIController, :create
    get "/getdest", UserAPIController, :getdest
  end

  scope "/api", ParkxWeb do
    pipe_through [:api, :browser_auth]
    delete "/sessions/:id", SessionAPIController, :delete
    get "/searchparking", UserAPIController, :searchparking
    get "/getuser", UserAPIController, :getuser
    get "/mybookings", BookingAPIController, :mybookings
    post "/hourlybooking", BookingAPIController, :hourlybooking
    post "/realtimebooking", BookingAPIController, :realtimebooking
    post "/realtimebookingend", BookingAPIController, :realtimebookingend
    post "/hourbookingend", BookingAPIController, :hourbookingend
    post "/hourbookingextend", BookingAPIController, :hourbookingextend
  end
end

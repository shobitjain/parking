defmodule ParkxWeb.LinksChannel do
  use Phoenix.Channel

  def join("links", _payload, socket) do
    {:ok, socket}
  end

  end
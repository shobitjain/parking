defmodule ParkxWeb.SessionAPIController do
  use ParkxWeb, :controller
  alias Parkx.{Repo, Entities.User, Auth.Guardian}
  alias Parkx.Authentication

  def create(conn, %{"username" => username, "password" => password}) do
    user = Repo.get_by(User, username: username)
    case Authentication.check_credentials(user, password) do
      {:ok, _} ->
        {:ok, jwt, _full_claims} = Guardian.encode_and_sign(user)
        conn
        |> put_status(200)
        |> json(%{token: jwt, user_id: user.id, credit: user.credit, username: user.username, message: "Logged In"})
      {:error, _} ->
        conn
        |> put_status(400)
        |> json(%{message: "Bad credentials"})
    end
  end

  def delete(conn, _params) do
    conn
    |> Guardian.Plug.current_token
    |> Guardian.revoke

    conn
    |> put_status(200)
    |> json(%{msg: "Good bye"})
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_status(403)
    |> json(%{msg: "You are not logged in"})
  end
end

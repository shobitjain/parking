defmodule ParkxWeb.PageController do
  use ParkxWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end

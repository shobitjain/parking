defmodule ParkxWeb.BookingAPIController do
  use ParkxWeb, :controller
  use Timex

  import Ecto.Query, only: [from: 2]
  alias Parkx.Repo
  alias Ecto.{Changeset, Multi}
  alias Parkx.{Repo, Entities.User, Model.Helper, Entities.Parking, Entities.Booking}

  def mybookings(conn, _params) do
    user = Parkx.Authentication.load_current_user(conn)
    query = from b in Booking,
            join: p in Parking, on: b.parking_id == p.id,
            where: b.user_id == ^user.id,
            select: {b, p.dest_name}

    bookings = Repo.all(query)
               |> Enum.map(fn {x,y} -> Map.from_struct(x) |> Map.put(:dest_name, y) end)
               |> Enum.map(fn x -> Map.take(x, [:id, :start_time, :end_time, :charge, :payment_status, :status, :type, :user_id, :parking_id, :dest_name]) end)

    conn
    |> put_status(200)
    |> json(bookings)
end

  def hourlybooking(conn, params) do

    user = Parkx.Authentication.load_current_user(conn)
    zone = params["zone"]
    hours = params["hours"]
    parkingId = params["parking_id"]
    query = from b in Booking,
            where: b.user_id == ^user.id and b.parking_id == ^parkingId and b.status == "ongoing",
            select: b
    dup_booking = Repo.all(query)
    if length(dup_booking) >= 1 do
      conn
      |> put_status(400)
      |> json(%{status: "You already have an ongoing booking for this spot"})
    else
      case zone do
        "A" ->
          charge = 2*hours
          responseConn = updateUserCredit(conn,user,charge,params)
          responseConn
        "B" ->
          charge = 1*hours
          responseConn = updateUserCredit(conn,user,charge,params)
          responseConn
      end
    end

  end

  def realtimebooking(conn, params) do
    user = Parkx.Authentication.load_current_user(conn)
    parkingId = params["parking_id"]
    parking = Repo.one(from p in Parking, where: p.id == ^parkingId)
    startTime = DateTime.utc_now() |> Timex.shift(minutes: 120)
    {:ok, starttime_str} = Timex.format(startTime, "{ISO:Extended}")
    query = from b in Booking,
            where: b.user_id == ^user.id and b.parking_id == ^parkingId and b.status == "ongoing",
            select: b
    dup_booking = Repo.all(query)
    if length(dup_booking) >= 1 do
      conn
      |> put_status(400)
      |> json(%{status: "You already have an ongoing booking for this spot"})
    else
      if user.credit >= 10.0 do
      changeset = Booking.changeset(%Booking{}, %{})
                  |> Changeset.put_change(:start_time, starttime_str)
                  |> Changeset.put_change(:status, "ongoing")
                  |> Changeset.put_change(:type, "realtime")
                  |> Changeset.put_change(:payment_status, "unpaid")
                  |> Changeset.put_change(:parking_id, String.to_integer(params["parking_id"]))
                  |> Changeset.put_change(:user_id, user.id)
      booking2 = Repo.insert!(changeset)
      Multi.new
      |> Multi.update(:parking, Parking.changeset(parking, %{status: "inuse"}))
      |> Repo.transaction
      conn
          |> put_status(200)
          |> json(%{status: "Booking started successfully.", booking_id: booking2.id})
      else
        conn
        |> put_status(400)
        |> json(%{status: "Your balance is below 10 credit. Please Recharge to make booking."})
      end
    end
  end

  def realtimebookingend(conn, params) do

    user = Parkx.Authentication.load_current_user(conn)
    parkingId = params["parking_id"]
    bookingId = params["booking_id"]
    booking = Repo.one(from b in  Booking, where: b.parking_id == ^parkingId and b.id == ^bookingId)
    parking = Repo.one(from p in Parking, where: p.id == ^parkingId)

    startTime = Timex.parse!(booking.start_time, "{ISO:Extended:Z}")
    endTime = DateTime.utc_now() |> Timex.shift(minutes: 120)
    diff = Timex.diff(endTime, startTime, :seconds)

    # hourCycle = diff/(60*60) |> Float.ceil |> trunc
    minuteCycle = diff/(5*60) |> Float.ceil |> trunc
    zone = parking.zone
    if booking.status == "completed" do
      conn
      |> put_status(400)
      |> json(%{status: "The Booking has already been closed"})
    else
        case zone do
          "A" ->
            charge = 0.16 * minuteCycle
            responseConn = updateUserCreditRealTime(conn,user,charge,parking,booking)
            responseConn

          "B" ->
            charge = 0.08 * minuteCycle
            responseConn = updateUserCreditRealTime(conn,user,charge,parking,booking)
            responseConn

        end
    end
  end

  def hourbookingend(conn, params) do
    new_endTime = DateTime.utc_now() |> Timex.shift(minutes: 120)
    {:ok, new_endtime_str} = Timex.format(new_endTime, "{ISO:Extended}")
    parkingId = params["parking_id"]
    bookingId = params["booking_id"]
    userId = params["user_id"]
    booking = Repo.one(from b in  Booking, where: b.parking_id == ^parkingId and b.id == ^bookingId)
    parking = Repo.one(from p in Parking, where: p.id == ^parkingId)
    user = Repo.one(from u in User, where: u.id == ^userId)
    startTime = Timex.parse!(booking.start_time, "{ISO:Extended:Z}")
    new_diff = Timex.diff(new_endTime, startTime, :seconds)

    new_hourCycle = new_diff/(60*60) |> Float.ceil |> trunc
    zone = parking.zone
    case zone do
      "A" ->
        newCharge = new_hourCycle * 2
        userRefund = booking.charge - newCharge
        updatedcredit = user.credit + userRefund
        Multi.new
          |> Multi.update(:booking, Booking.changeset(booking, %{ end_time: new_endtime_str, status: "completed", charge: newCharge}))
          |> Multi.update(:parking, Parking.changeset(parking, %{status: "free"}))
          |> Multi.update(:user, User.changeset(user, %{credit: updatedcredit}))
          |> Repo.transaction
      "B" ->
        newCharge = new_hourCycle * 1
        userRefund = booking.charge - newCharge
        updatedcredit = user.credit + userRefund
        Multi.new
          |> Multi.update(:booking, Booking.changeset(booking, %{ end_time: new_endtime_str, status: "completed", charge: newCharge}))
          |> Multi.update(:parking, Parking.changeset(parking, %{status: "free"}))
          |> Multi.update(:user, User.changeset(user, %{credit: updatedcredit}))
          |> Repo.transaction
    end
    conn
    |> put_status(200)
    |> json(%{status: "Hourly Booking ended successfully."})

  end

    def updateUserCredit(conn,user,charge,params) do
          startTime = DateTime.utc_now() |> Timex.shift(minutes: 120)
          min =  params["hours"]*60
          endTime = DateTime.utc_now() |> Timex.shift(minutes: 120) |> Timex.shift(minutes: min)
          {:ok, endtime_str} = Timex.format(endTime, "{ISO:Extended}")
          {:ok, starttime_str} = Timex.format(startTime, "{ISO:Extended}")
          parkingId = params["parking_id"]
          parking = Repo.one(from p in Parking, where: p.id == ^parkingId)
          if user.credit >= charge do
            updatedcredit = user.credit-charge
            Multi.new
            |> Multi.update(:user, User.changeset(user, %{credit: updatedcredit}))
            |> Repo.transaction

            changeset = Booking.changeset(%Booking{}, %{})
                            |> Changeset.put_change(:start_time, starttime_str)
                            |> Changeset.put_change(:end_time, endtime_str)
                            |> Changeset.put_change(:payment_status, "paid")
                            |> Changeset.put_change(:status, "ongoing")
                            |> Changeset.put_change(:type, "hourly")
                            |> Changeset.put_change(:charge, charge/1)
                            |> Changeset.put_change(:user_id, user.id)
                            |> Changeset.put_change(:parking_id, String.to_integer(params["parking_id"]))

                Repo.insert!(changeset)
            Multi.new
            |> Multi.update(:parking, Parking.changeset(parking, %{status: "inuse"}))
            |> Repo.transaction
            conn
            |> put_status(200)
            |> json(%{status: "Booking done successfully."})
          else
            conn
            |> put_status(400)
            |> json(%{status: "Not Enough Credit Please reload credit"})
          end
        end

    def updateUserCreditRealTime(conn,user,charge,parking,booking) do
      endTime = DateTime.utc_now() |> Timex.shift(minutes: 120)
      {:ok, endtime_str} = Timex.format(endTime, "{ISO:Extended}")

      result = Multi.new
      |> Multi.update(:booking, Booking.changeset(booking, %{end_time: endtime_str, charge: charge, status: "completed"}))
      |> Multi.update(:parking, Parking.changeset(parking, %{status: "free"}))
      |> Repo.transaction
      if !user.monthly_selected do
        updatedcredit = user.credit-charge
        booking = booking |> Booking.changeset(%{payment_status: "paid"}) |> Repo.update!()
        user = user |> User.changeset(%{credit: updatedcredit}) |> Repo.update!()
          else
        booking = booking |> Booking.changeset(%{payment_status: "unpaid"}) |> Repo.update!()
      end
      case result do
        {:ok, _op} ->
          conn
          |> put_status(200)
          |> json(%{status: "Realtime Booking ended successfully."})
        {:error, _op} ->
          conn
          |> put_status(400)
          |> json(%{status: "Failed to end Real time booking."})
      end
    end

    def hourbookingextend(conn,params) do
      user = Parkx.Authentication.load_current_user(conn)
      {_a, b} = Enum.at(conn.req_headers, 1)
      hours = params["hours"]
      bookingId = params["booking_id"]
      query = from b in Booking,
                   where: b.id == ^bookingId,
                   select: b
      dup_booking = Repo.all(query)
      parking = Repo.one(from p in Parking, where: p.id == ^hd(dup_booking).parking_id)
      zone = parking.zone
      case zone do
        "A" ->
          charge = 2*String.to_integer(hours)
          res = updateforextend(conn,dup_booking,charge,hours)
          res
        "B" ->
          charge = 1*String.to_integer(hours)
          res = updateforextend(conn,dup_booking,charge,hours)
          res
      end

    end

    def updateforextend(conn,dup_booking,charge,hours) do
      user = Parkx.Authentication.load_current_user(conn)
        if length(dup_booking) == 1 and user.credit >= charge do
          updatedcredit = user.credit-charge
          Multi.new
          |> Multi.update(:user, User.changeset(user, %{credit: updatedcredit}))
          |> Repo.transaction
      currBookingEndTime = hd(dup_booking).end_time
      inMin = String.to_integer(hours) * 60
      extendedBookingEndTime = Timex.parse!(currBookingEndTime, "{ISO:Extended:Z}") |> Timex.shift(minutes: inMin)
      {:ok, endtime_str} = Timex.format(extendedBookingEndTime, "{ISO:Extended}")
      bookingUpdated = hd(dup_booking) |> Booking.changeset(%{end_time: endtime_str}) |> Repo.update!()
      bookingUpdated = hd(dup_booking) |> Booking.changeset(%{charge: hd(dup_booking).charge+charge}) |> Repo.update!()
      conn
      |> put_status(200)
      |> json(%{status: "Your booking extended successfully"})
    else
      conn
      |> put_status(400)
      |> json(%{status: "Error while extending booking user credit is not enough"})
    end
  end

end

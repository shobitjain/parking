defmodule ParkxWeb.UserAPIController do
  use ParkxWeb, :controller

  import Ecto.Query, only: [from: 2]
  alias Parkx.{Repo, Entities.User, Model.Helper, Entities.Parking}

  def create(conn, params) do
    changeset = User.changeset(%User{}, params["user"])
    case Repo.insert(changeset) do
      {:ok, _user} ->
        conn
        |> put_status(200)
        |> json(%{message: "user registered successfully."})
      {:error, _} ->
        conn
        |> put_status(400)
        |> json(%{message: "Bad Data."})
    end

  end

  def getdest(conn, %{"lat" => lat, "lng" => lng}) do

    case (GoogleMaps.geocode({String.to_float(lat), String.to_float(lng)})) do
      {:ok, googleresp} ->
        [head | _tail] = googleresp["results"]
        dest = head["formatted_address"]
        conn
        |> put_status(200)
        |> json(%{status: "ok", destination: dest})
      {:error, _resp} ->
        conn
        |> put_status(400)
        |> json(%{status: "Invalid destination entered"})
    end
  end

  def searchparking(conn, %{"destination" => dest, "endTime" => endTime}) do

    maxParkingDistance = 1000

    case (GoogleMaps.geocode(dest)) do

      {:ok, googleresp} ->
        [head | _tail] = googleresp["results"]
        %{"location" => coord} = head["geometry"]
        {lat, lng} = {coord["lat"], coord["lng"]}

        query = from p in Parking, where: p.status == "free", select: p

        spots = Repo.all(query)
        |> Enum.map(fn x -> Map.from_struct(x) end)
        |> Enum.map(fn x -> Map.take(x, [:title, :dest_name, :lattitude, :longitude, :zone, :status, :id]) end)
        |> Enum.filter(fn %{lattitude: x, longitude: y} -> Helper.getDistance(lat, lng, x, y) < maxParkingDistance end)
        |> Enum.map(fn x -> Helper.changeDestLatLong(x) end)

        returnRes = Helper.addPrice(spots, endTime)

        case returnRes[:status] do
          "ok" ->
            conn
            |> put_status(200)
            |> json(returnRes)
          "EndTime error" ->
            conn
            |> put_status(200)
            |> json(returnRes)
        end


      {:error, _resp} ->
        conn
        |> put_status(400)
        |> json(%{status: "Invalid destination entered"})

    end

  end

  def getuser(conn, _params) do
    user = Parkx.Authentication.load_current_user(conn)
    conn
    |> put_status(200)
    |> json(%{status: "current user returned successfully", credit: user.credit, username: user.username})
  end

end

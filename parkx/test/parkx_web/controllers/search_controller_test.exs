defmodule ParkxWeb.SearchControllerTest do
  use ParkxWeb.ConnCase, async: true
  import Ecto.Query, only: [from: 2]
  alias Parkx.Auth.Guardian
  alias Parkx.Repo
  alias Parkx.Entities.User
  alias Parkx.Entities.Parking

  # import Ecto.Query, only: [from: 2]

  setup %{conn: conn} do
    changeset = User.changeset(%User{}, %{name: "Smith Jacobs", username: "smith", hashed_password: "parool"})
    Repo.insert!(changeset)
    user = Repo.get_by(User, username: "smith")
    # create the token
    {:ok, jwt, _full_claims} = Guardian.encode_and_sign(user)

    # add authorization header to request
    conn = conn |> put_req_header("authorization", "Bearer #{jwt}")
    |> put_req_header("accept", "application/json")

    # pass the connection and the user to the test
    {:ok, conn: conn, user: user}
  end

  test "The system presents a summary of available parking space around that address and not all the parking spots ", %{conn: conn} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, A piirkond", dest_name: "Vanemuise 4, Tartu, 51003", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "A", status: "free"})
    Repo.insert!(changeset1)
    changeset2 = Parking.changeset(%Parking{}, %{status: "inuse"})
    Repo.insert!(changeset2)
    totalSpots = Repo.all(from p in Parking, select: p)
    conn = get conn, "/api/searchparking", %{"destination" => "Vanemuise 4, Tartu, 51003", "endTime" => ""}
    res = json_response(conn, 200)
    availParkingNum = length(res["tabledata"])
    totalParkingNum = length(totalSpots)
    assert availParkingNum != totalParkingNum
  end

  test "system shows the space availability and information about the price that applies (Zone A vs B)", %{conn: conn} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, A piirkond", dest_name: "Vanemuise 4, Tartu, 51003", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "A", status: "free"})
    Repo.insert!(changeset1)
    changeset2 = Parking.changeset(%Parking{}, %{title: "Tartu linn, A piirkond", dest_name: "Kesklinn", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "B", status: "free"})
    Repo.insert!(changeset2)
    conn = get conn, "/api/searchparking", %{"destination" => "Vanemuise 4, Tartu, 51003", "endTime" => "2019-12-20T10:40:00.658Z"}
    assert conn.status == 200
    res = json_response(conn, 200)
    zoneA = Enum.at(res["tabledata"], 0)
    zoneB = Enum.at(res["tabledata"], 1)
    assert zoneA["status"] == "free"
    assert zoneB["status"] == "free"
    assert zoneA["HourlyCharge"] == 2 * zoneB["HourlyCharge"]
    assert zoneA["RealTimeCharge"] == 2 * zoneB["RealTimeCharge"]
  end

  test "Search parking space with blank endTime", %{conn: conn} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, A piirkond", dest_name: "Vanemuise 4, Tartu, 51003", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "A", status: "free"})
    Repo.insert!(changeset1)
    changeset2 = Parking.changeset(%Parking{}, %{status: "inuse"})
    Repo.insert!(changeset2)
    totalSpots = Repo.all(from p in Parking, select: p)
    totalParkingNum = length(totalSpots)
    conn = get conn, "/api/searchparking", %{"destination" => "Vanemuise 4, Tartu, 51003", "endTime" => "2019-12-20T10:40:00.658Z"}
    assert conn.status == 200
    res = json_response(conn, 200)
    assert res["status"] == "ok"
    [head | _tail] = res["tabledata"]
    dest_name = head["Parking_spot"]
    assert dest_name == "Vanemuise 4, Tartu, 51003"
  end

  test "Search parking space with endTime", %{conn: conn} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, A piirkond", dest_name: "Vanemuise 4, Tartu, 51003", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "A", status: "free"})
    Repo.insert!(changeset1)
    conn = get conn, "/api/searchparking", %{"destination" => "Vanemuise 4, Tartu, 51003", "endTime" => "2019-11-20T10:40:00.658Z"}
    assert conn.status == 200
    res = json_response(conn, 200)
    assert res["status"] == "EndTime error"
    [head | _tail] = res["tabledata"]
    dest_name = head["Parking_spot"]
    assert dest_name == "Vanemuise 4, Tartu, 51003"
  end

  test "Search parking space with invalid input as destination address", %{conn: conn} do
    changeset = Parking.changeset(%Parking{}, %{title: "Tartu linn, A piirkond", dest_name: "Vanemuise 4, Tartu, 51003", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "A", status: "free"})
    Repo.insert!(changeset)
    conn = get conn, "/api/searchparking", %{"destination" => "asdffasdfklljk", "endTime" => "2019-11-21T10:40:00.658Z"}
    assert conn.status == 400
    res = json_response(conn, 400)
    assert res["status"] == "Invalid destination entered"
  end

  test "The system provides an estimation of the fee in hourly payment", %{conn: conn} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, A piirkond", dest_name: "Vanemuise 4, Tartu, 51003", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "A", status: "free"})
    Repo.insert!(changeset1)
    changeset2 = Parking.changeset(%Parking{}, %{title: "Tartu linn, A piirkond", dest_name: "Kesklinn", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "B", status: "free"})
    Repo.insert!(changeset2)
    conn1 = get conn, "/api/searchparking", %{"destination" => "Vanemuise 4, Tartu, 51003", "endTime" => "2019-12-15T10:40:00.658Z"}
    conn2 = get conn, "/api/searchparking", %{"destination" => "Vanemuise 4, Tartu, 51003", "endTime" => "2019-12-16T10:40:00.658Z"}
    res1 = json_response(conn1, 200)
    zone1A = Enum.at(res1["tabledata"], 0)
    zone1B = Enum.at(res1["tabledata"], 1)
    res2 = json_response(conn2, 200)
    zone2A = Enum.at(res2["tabledata"], 0)
    zone2B = Enum.at(res2["tabledata"], 1)
    assert zone1A["HourlyCharge"] == 2 * zone1B["HourlyCharge"]
    assert zone1A["HourlyCharge"] < zone2A["HourlyCharge"]
  end

  test "The system provides an estimation of the fee in real-time payment", %{conn: conn} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, A piirkond", dest_name: "Vanemuise 4, Tartu, 51003", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "A", status: "free"})
    Repo.insert!(changeset1)
    changeset2 = Parking.changeset(%Parking{}, %{title: "Tartu linn, A piirkond", dest_name: "Kesklinn", lattitude: 58.37736740288578, longitude: 26.7249995470047, zone: "B", status: "free"})
    Repo.insert!(changeset2)
    conn1 = get conn, "/api/searchparking", %{"destination" => "Vanemuise 4, Tartu, 51003", "endTime" => "2019-12-15T10:40:00.658Z"}
    conn2 = get conn, "/api/searchparking", %{"destination" => "Vanemuise 4, Tartu, 51003", "endTime" => "2019-12-16T10:40:00.658Z"}
    res1 = json_response(conn1, 200)
    zone1A = Enum.at(res1["tabledata"], 0)
    zone1B = Enum.at(res1["tabledata"], 1)
    res2 = json_response(conn2, 200)
    zone2A = Enum.at(res2["tabledata"], 0)
    zone2B = Enum.at(res2["tabledata"], 1)
    assert zone1A["RealTimeCharge"] == 2 * zone1B["RealTimeCharge"]
    assert zone1A["RealTimeCharge"] < zone2A["RealTimeCharge"]
  end

end

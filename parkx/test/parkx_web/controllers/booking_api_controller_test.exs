defmodule ParkxWeb.BookingControllerTest do
  use ParkxWeb.ConnCase, async: true

  import Ecto.Query, only: [from: 2]
  alias Ecto.{Changeset, Multi}
  alias Parkx.Auth.Guardian
  alias Parkx.Repo
  alias Parkx.Entities.User
  alias Parkx.Entities.Parking
  alias Parkx.Entities.Booking
  alias Ecto.{Changeset, Multi}

  # import Ecto.Query, only: [from: 2]

  setup %{conn: conn} do
    changeset = User.changeset(%User{}, %{name: "Smith Jacobs", username: "smith", hashed_password: "parool", credit: 100})
    Repo.insert!(changeset)
    user = Repo.get_by(User, username: "smith")

    # create the token
    {:ok, jwt, _full_claims} = Guardian.encode_and_sign(user)

    # add authorization header to request
    conn = conn |> put_req_header("authorization", "Bearer #{jwt}")
           |> put_req_header("accept", "application/json")

    # pass the connection and the user to the test
    {:ok, conn: conn, user: user}
  end

  test "The system allows a car driver to select between hourly or real-time payment", %{conn: conn, user: user} do
    changeset = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking = Repo.insert!(changeset)

    conn = post conn, "/api/hourlybooking", %{parking_id: "#{parking.id}", hours: 2, zone: "A" }
    query1 = from b in Booking, where: b.parking_id == ^parking.id, select: b
    booking = Repo.one(query1)
    assert booking.type == "hourly"

    # changeset2 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 23, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    # parking2 = Repo.insert!(changeset2)
    # conn2 = post conn, "/api/realtimebooking", %{parking_id: "#{parking2.id}"}
    # res = json_response(conn2, 200)

    # query2 = from b in Booking, where: b.parking_id == ^parking2.id, select: b
    # booking2 = Repo.one(query2)
    # assert booking2.type == "realtime"
  end

  test "The system allows a car driver to submit a start and end of parking time", %{conn: conn, user: user} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking1 = Repo.insert!(changeset1)
    conn1 = post conn, "/api/realtimebooking", %{parking_id: "#{parking1.id}"}
    res1 = json_response(conn1, 200)
    query1 = from b in Booking, where: b.parking_id == ^parking1.id, select: b
    booking = Repo.one(query1)

    :timer.sleep(1000)
    conn2 = post conn, "/api/realtimebookingend", %{parking_id: "#{parking1.id}", booking_id: "#{booking.id}"}
    booking2 = Repo.one(query1)
    startTime = Timex.parse!(booking2.start_time, "{ISO:Extended:Z}")
    endTime = Timex.parse!(booking2.end_time, "{ISO:Extended:Z}")
    diff = Timex.diff(endTime, startTime, :seconds)
    assert diff == 1
  end

  test "Hourly Booking Successful", %{conn: conn} do
    changeset = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking = Repo.insert!(changeset)
    conn = post conn, "/api/hourlybooking", %{parking_id: "#{parking.id}", hours: 2, zone: "A" }
    assert conn.status == 200
    res = json_response(conn, 200)
    assert res["status"] == "Booking done successfully."
  end

  test "The system blocks the corresponding parking space and updates the availability of such parking space", %{conn: conn} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking1 = Repo.insert!(changeset1)
    conn1 = post conn, "/api/hourlybooking", %{parking_id: "#{parking1.id}", hours: 2, zone: "A" }
    res1 = json_response(conn1, 200)
    query1 = from p in Parking, where: p.id == ^parking1.id, select: p
    updatedParking1 = Repo.one(query1)
    assert updatedParking1.status == "inuse"

    changeset2 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 20, Tartu, 51007", lattitude: "59.38502944712024", longitude: "27.710392236709595", zone: "A", status: "free"})
    parking2 = Repo.insert!(changeset2)
    conn2 = post conn, "/api/realtimebooking", %{parking_id: "#{parking2.id}"}
    res2 = json_response(conn2, 200)
    query2 = from p in Parking, where: p.id == ^parking2.id, select: p
    updatedParking2 = Repo.one(query2)
    assert updatedParking2.status == "inuse"
  end

  test "Hourly Booking Not Enough Credit", %{conn: conn, user: user} do
    updatedUser = user |> User.changeset(%{credit: 1.0}) |> Repo.update!()
    conn = post conn, "/api/hourlybooking", %{parking_id: "1", hours: 3, zone: "A" }
    assert conn.status == 400
    res = json_response(conn, 400)
    assert res["status"] == "Not Enough Credit Please reload credit"
  end

  test "Hourly Booking Successful zone b", %{conn: conn} do
    changeset = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking = Repo.insert!(changeset)
    conn = post conn, "/api/hourlybooking", %{parking_id: "#{parking.id}", hours: 2, zone: "B" }
    assert conn.status == 200
    res = json_response(conn, 200)
    assert res["status"] == "Booking done successfully."
  end

  test "Hourly Booking Not Enough Credit zone b", %{conn: conn, user: user} do
    updatedUser = user |> User.changeset(%{credit: 1.0}) |> Repo.update!()
    conn = post conn, "/api/hourlybooking", %{parking_id: "1", hours: 3, zone: "B" }
    assert conn.status == 400
    res = json_response(conn, 400)
    assert res["status"] == "Not Enough Credit Please reload credit"
  end

  test "Realtime Booking started successfully", %{conn: conn} do
    changeset = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking = Repo.insert!(changeset)
    conn = post conn, "/api/realtimebooking", %{parking_id: "#{parking.id}"}
    assert conn.status == 200
    res = json_response(conn, 200)
    assert res["status"] == "Booking started successfully."
  end

  test "Realtime Booking not started because not enough credit", %{conn: conn , user: user} do
    updatedUser = user |> User.changeset(%{credit: 1.0}) |> Repo.update!()
    changeset = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking = Repo.insert!(changeset)
    conn = post conn, "/api/realtimebooking", %{parking_id: "#{parking.id}"}
    assert conn.status == 400
    res = json_response(conn, 400)
    assert res["status"] == "Your balance is below 10 credit. Please Recharge to make booking."
  end

  test "Realtime Booking ended successfully without monthly selected", %{conn: conn , user: user} do
    updatedUser = user |> User.changeset(%{momonthly_selected: false}) |> Repo.update!()
    changeset = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking = Repo.insert!(changeset)
    changesetb = Booking.changeset(%Booking{}, %{start_time: "2019-11-28T18:14:14.559000+00:00", charge: 0.0, payment_status: "unpaid", type: "realtime", status: "ongoing"})
                 |> Changeset.put_change(:user_id, updatedUser.id) |> Changeset.put_change(:parking_id, parking.id)
    booking = Repo.insert!(changesetb)

    conn = post conn, "/api/realtimebookingend", %{parking_id: "#{parking.id}", booking_id: "#{booking.id}"}
    assert conn.status == 200
    res = json_response(conn, 200)
    assert res["status"] == "Realtime Booking ended successfully."
  end

  test "Realtime Booking ended successfully with monthly selected", %{conn: conn, user: user} do
    updatedUser = user |> User.changeset(%{monthly_selected: true}) |> Repo.update!()
    changeset = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking = Repo.insert!(changeset)
    changesetb = Booking.changeset(%Booking{}, %{start_time: "2019-11-28T18:30:14.559000+00:00", charge: 0.0, payment_status: "unpaid", type: "realtime", status: "ongoing"})
                |> Changeset.put_change(:user_id, updatedUser.id) |> Changeset.put_change(:parking_id, parking.id)
    booking = Repo.insert!(changesetb)

    conn = post conn, "/api/realtimebookingend", %{parking_id: "#{parking.id}", booking_id: "#{booking.id}"}
    assert conn.status == 200
    res = json_response(conn, 200)
    assert res["status"] == "Realtime Booking ended successfully."
  end

  test "Booking already exists", %{conn: conn, user: user} do
    changeset = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking = Repo.insert!(changeset)
    post conn, "/api/hourlybooking", %{parking_id: "#{parking.id}", hours: 2, zone: "B" }
    conn = post conn, "/api/hourlybooking", %{parking_id: "#{parking.id}", hours: 2, zone: "B" }
    assert conn.status == 400
    res = json_response(conn, 400)
    assert res["status"] == "You already have an ongoing booking for this spot"
  end

  test "Hourly Booking Extended Successful", %{conn: conn, user: user} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking1 = Repo.insert!(changeset1)
    conn1 = post conn, "/api/hourlybooking", %{parking_id: "#{parking1.id}", hours: 5, zone: "A" }
    res1 = json_response(conn1, 200)
    query1 = from b in Booking, where: b.parking_id == ^parking1.id, select: b
    booking = Repo.one(query1)

    conn2 = post conn, "/api/hourbookingextend", %{booking_id: "#{booking.id}", hours: "5" }
    booking2 = Repo.one(query1)
    assert conn2.status == 200
    res = json_response(conn2, 200)
    assert res["status"] == "Your booking extended successfully"
  end

  test "Hourly Booking Extended with error", %{conn: conn, user: user} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking1 = Repo.insert!(changeset1)
    userUpdated = user |> User.changeset(%{credit: 14}) |> Repo.update!()
    conn1 = post conn, "/api/hourlybooking", %{parking_id: "#{parking1.id}", hours: 5, zone: "A" }

    query1 = from b in Booking, where: b.parking_id == ^parking1.id, select: b
    booking = Repo.one(query1)
    conn2 = post conn, "/api/hourbookingextend", %{booking_id: "#{booking.id}", hours: "5" }

    assert conn2.status == 400
    res = json_response(conn2, 400)
    assert res["status"] == "Error while extending booking user credit is not enough"
  end

  test "The driver can extend the parking period", %{conn: conn, user: user} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking1 = Repo.insert!(changeset1)
    conn1 = post conn, "/api/hourlybooking", %{parking_id: "#{parking1.id}", hours: 5, zone: "A" }
    res1 = json_response(conn1, 200)
    query1 = from b in Booking, where: b.parking_id == ^parking1.id, select: b
    booking = Repo.one(query1)
    conn2 = post conn, "/api/hourbookingextend", %{booking_id: "#{booking.id}", hours: "5" }
    booking2 = Repo.one(query1)
    timeShiftManually = Timex.parse!(booking.start_time, "{ISO:Extended:Z}") |> Timex.shift(hours: 10)
    afterExtEndTime = Timex.parse!(booking2.end_time, "{ISO:Extended:Z}")
    diff = Timex.diff(timeShiftManually, afterExtEndTime, :minute)
    assert diff == 0
    assert booking2.charge == 2*booking.charge
  end

end

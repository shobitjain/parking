defmodule ParkxWeb.UserApiControllerTest do
  use ParkxWeb.ConnCase, async: true

  alias Parkx.Auth.Guardian
  alias Parkx.Repo
  alias Parkx.Entities.User
  alias Parkx.Entities.Parking



  test "User Registration", %{conn: conn} do
    conn = post conn, "/api/users", %{user: [name: "hariti n", username: "fred", password: "parool",email: "abc@gmail.com",credit: "2.5",monthly_selected: true]}
    assert json_response(conn, 200) == %{"message" => "user registered successfully."}
  end

  test "Registration submission with failure", %{conn: conn} do
    conn = post conn, "/api/users", %{user: [username: "fred", hashed_password: "parool", email: "abc@gmail.com", credit: 2.1, monthly_selected: false]}
    assert conn.status == 400
    res = json_response(conn, 400)
    assert res["message"] == "Bad Data."
  end



end


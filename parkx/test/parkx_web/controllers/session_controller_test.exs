defmodule ParkxWeb.SessionControllerTest do
  use ParkxWeb.ConnCase

  alias Parkx.Repo
  alias Parkx.Entities.User

  import Ecto.Query, only: [from: 2]


  test "Login with success", %{conn: conn} do
    changeset = User.changeset(%User{}, %{name: "Fred Flintstone", username: "fred", hashed_password: "parool"})
    Repo.insert!(changeset)
    conn = post conn, "/api/sessions", %{username: "fred", password: "parool"}
    assert conn.status == 200
    res = json_response(conn, 200)
    assert res["message"] == "Logged In"
  end

  test "Login with failure", %{conn: conn} do
    changeset = User.changeset(%User{}, %{name: "Fred Flintstone", username: "fred", hashed_password: "parool"})
    Repo.insert!(changeset)
    conn = post conn, "/api/sessions", %{username: "fred", password: "paroo"}
    assert conn.status == 400
    res = json_response(conn, 400)
    assert res["message"] == "Bad credentials"
  end


end

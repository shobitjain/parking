defmodule ParkxWeb.PaymentTest do
  use ParkxWeb.ConnCase, async: true

  import Ecto.Query, only: [from: 2]
  alias Ecto.{Changeset, Multi}
  alias Parkx.Auth.Guardian
  alias Parkx.Repo
  alias Parkx.Entities.User
  alias Parkx.Entities.Parking
  alias Parkx.Entities.Booking
  alias Ecto.{Changeset, Multi}

  # import Ecto.Query, only: [from: 2]

  setup %{conn: conn} do
    changeset = User.changeset(%User{}, %{name: "Smith Jacobs", username: "smith", hashed_password: "parool", credit: 100})
    Repo.insert!(changeset)
    user = Repo.get_by(User, username: "smith")

    # create the token
    {:ok, jwt, _full_claims} = Guardian.encode_and_sign(user)

    # add authorization header to request
    conn = conn |> put_req_header("authorization", "Bearer #{jwt}")
           |> put_req_header("accept", "application/json")

    # pass the connection and the user to the test
    {:ok, conn: conn, user: user}
  end

  test "(On hourly-based payment) The system allows the driver to pay before starting the parking period.", %{conn: conn, user: user} do
    changeset = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking = Repo.insert!(changeset)
    conn = post conn, "/api/hourlybooking", %{parking_id: "#{parking.id}", hours: 2, zone: "A" }
    res = json_response(conn, 200)
    assert res["status"] == "Booking done successfully."
    query1 = from u in User, where: u.id == ^user.id, select: u
    updatedUser = Repo.one(query1)
    assert user.credit == updatedUser.credit + 4
  end

  test "(On hourly-based payment) The system allows the driver to pay when extending the parking period.", %{conn: conn, user: user} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking1 = Repo.insert!(changeset1)
    queryUser = from u in User, where: u.id == ^user.id, select: u

    conn1 = post conn, "/api/hourlybooking", %{parking_id: "#{parking1.id}", hours: 5, zone: "A" }
    res1 = json_response(conn1, 200)
    updatedUser1 = Repo.one(queryUser)
    query1 = from b in Booking, where: b.parking_id == ^parking1.id, select: b
    booking = Repo.one(query1)

    conn2 = post conn, "/api/hourbookingextend", %{booking_id: "#{booking.id}", hours: "5" }
    booking2 = Repo.one(query1)
    assert conn2.status == 200
    updatedUser2 = Repo.one(queryUser)
    res = json_response(conn2, 200)

    assert updatedUser1.credit == updatedUser2.credit + 10
    assert user.credit == updatedUser2.credit + 20
  end

  test "(On real-time payment) The system allows the driver to pay at the end of the parking period", %{conn: conn, user: user} do
    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking1 = Repo.insert!(changeset1)
    conn1 = post conn, "/api/realtimebooking", %{parking_id: "#{parking1.id}"}
    res1 = json_response(conn1, 200)
    queryUser = from u in User, where: u.id == ^user.id, select: u
    updatedUser1 = Repo.one(queryUser)

    query1 = from b in Booking, where: b.parking_id == ^parking1.id, select: b
    booking = Repo.one(query1)

    :timer.sleep(1000)
    conn2 = post conn, "/api/realtimebookingend", %{parking_id: "#{parking1.id}", booking_id: "#{booking.id}"}
    updatedUser2 = Repo.one(queryUser)

    initialCredit = user.credit
    creditAfterBookingStart = updatedUser1.credit
    creditAfterBookingEnd = updatedUser2.credit

    assert creditAfterBookingStart == initialCredit
    assert creditAfterBookingEnd == initialCredit - 0.16
  end

  test "(On real-time payment) The system allows the driver to configure the option of paying at the end of the month", %{conn: conn, user: user} do
    conn = post conn, "/api/users", %{user: [name: "mark", username: "mark", password: "parool",email: "abc@gmail.com",credit: "200",monthly_selected: true]}
    assert json_response(conn, 200) == %{"message" => "user registered successfully."}
    conn2 = post conn, "/api/users", %{user: [name: "ben", username: "ben", password: "parool",email: "abc@gmail.com",credit: "100",monthly_selected: false]}
    assert json_response(conn2, 200) == %{"message" => "user registered successfully."}
  end

  test "(On real-time payment) The system allows the driver to pay at the end of the month (if the option is enabled)", %{conn: conn, user: user} do
    updatedUser = user |> User.changeset(%{monthly_selected: true}) |> Repo.update!()

    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking1 = Repo.insert!(changeset1)
    conn1 = post conn, "/api/realtimebooking", %{parking_id: "#{parking1.id}"}
    res1 = json_response(conn1, 200)
    queryUser = from u in User, where: u.id == ^user.id, select: u
    updatedUser1 = Repo.one(queryUser)

    query1 = from b in Booking, where: b.parking_id == ^parking1.id, select: b
    booking = Repo.one(query1)

    :timer.sleep(1000)
    conn2 = post conn, "/api/realtimebookingend", %{parking_id: "#{parking1.id}", booking_id: "#{booking.id}"}
    updatedUser2 = Repo.one(queryUser)

    initialCredit = user.credit
    creditAfterBookingStart = updatedUser1.credit
    creditAfterBookingEnd = updatedUser2.credit

    assert creditAfterBookingStart == initialCredit
    assert creditAfterBookingEnd == initialCredit
  end

  test "The system guarantees not overlapping on the payment options", %{conn: conn, user: user} do

    changeset1 = Parking.changeset(%Parking{}, %{title: "Tartu linn, B piirkond", dest_name: "Tahtvere 26, Tartu, 51007", lattitude: "58.38502944712024", longitude: "26.710392236709595", zone: "A", status: "free"})
    parking1 = Repo.insert!(changeset1)
    queryUser = from u in User, where: u.id == ^user.id, select: u

    conn1 = post conn, "/api/hourlybooking", %{parking_id: "#{parking1.id}", hours: 2, zone: "A" }
    res1 = json_response(conn1, 200)
    updatedUser1 = Repo.one(queryUser)
    query1 = from b in Booking, where: b.parking_id == ^parking1.id, select: b
    booking = Repo.one(query1)

    conn2 = post conn, "/api/realtimebooking", %{parking_id: "#{parking1.id}"}
    booking2 = Repo.one(query1)
    res2 = json_response(conn2, 400)
    assert res2["status"] == "You already have an ongoing booking for this spot"

  end

end

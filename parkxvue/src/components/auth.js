import axios from 'axios';

export default {
  user: { role: '', username: '' },
  login: function (context, creds, redirect) {
    const baseUrl = process.env.VUE_APP_PHOENIX_URL || 'http://127.0.0.1:4000/api';
    axios.post(`${baseUrl}/sessions`, creds)
      .then(response => {
        this.user.username = creds.username;
        window.localStorage.setItem('token-jwt', response.data.token);
        window.localStorage.setItem('user-id', response.data.user_id);
        if(redirect) context.$router.push({ path: redirect });
        context.$toasted.success('Login Successful', {
          theme: 'bubble',
          position: 'top-right',
          duration: 5000
        });
      })
      .catch(error => {
        console.log(error);
        context.$toasted.error('Invalid username or password', {
          theme: 'bubble',
          position: 'top-right',
          duration: 5000
        });
      });
    },
    logout: function(context, options) {
      window.localStorage.removeItem('token-jwt');
      window.localStorage.removeItem('user-id');
      context.$router.push({ path: '/login' });
    },
    authenticated: function() {
      const jwt = window.localStorage.getItem('token-jwt');
      return !!jwt;
    },
    getAuthHeader: function() {
      return {
        Authorization: 'Bearer ' + window.localStorage.getItem('token-jwt')
      }
    }
}

import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home.vue'
import Register from '../components/Register.vue'
import Login from '../components/Login.vue'
import SearchParking from '../components/SearchParking.vue'
import ShowParking from '../components/ShowParking.vue'
import MyBooking from '../components/MyBooking.vue'


Vue.use(VueRouter)


function isLoggedIn() {
  const jwt = window.localStorage.getItem('token-jwt');
  return !!jwt;
}

function guard(to, from, next) {
  if(isLoggedIn() === true) {
      next();
  } else{
      next('/login');
  }
}

function logginRedirect(to, from, next) {
  if(isLoggedIn() === true) {
      next('/searchParking');
  } else{
      next();
  }
}

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    beforeEnter: logginRedirect
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
    beforeEnter: logginRedirect
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    beforeEnter: logginRedirect
  },
  {
    path: '/searchParking',
    name: 'searchParking',
    component: SearchParking,
    beforeEnter: guard
  },
  {
    path: '/showParking/:id',
    name: 'showParking',
    component: ShowParking,
    beforeEnter: guard
  },
  {
    path: '/mybooking',
    name: 'mybooking',
    component: MyBooking,
    beforeEnter: guard
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
